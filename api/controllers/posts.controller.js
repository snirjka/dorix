const postService = require('../services/post.service');

const createPost = async (req, res, next) => {
    const image = req.files.image;

    const post = await postService.create({
        description: req.fields.description,
        image
    });

    res.send({ status: true });
}

const updatePost = async (req, res) => {
    const { id } = req.params;
    await postService.update(id, {
        description: req.query.description
    });

    res.send({ status: true });
}

const removePost = async (req, res, next) => {
    const { id } = req.params;
    await postService.remove(id);

    res.send({ status: true });
}

const getPosts = async (req, res, next) => {
    const q = {};
    if (req.query.query) q.description = new RegExp(req.query.query, 'i');
    const posts = await postService.getAll(q);
    res.send(posts);
}

module.exports = {
    createPost,
    updatePost,
    getPosts,
    removePost
}