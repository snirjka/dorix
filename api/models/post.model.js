var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
    description: { type: String, required: true },
    imageUrl: { type: String, required: true },
}, { timestamps: true });

module.exports = mongoose.model('post', PostSchema);