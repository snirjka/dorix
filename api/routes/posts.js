var express = require('express');
var router = express.Router();
const postsController = require('../controllers/posts.controller');

router.get('/', postsController.getPosts);

router.post('/', postsController.createPost);

router.delete('/:id', postsController.removePost);

router.get('/:id', postsController.updatePost);

module.exports = router;
