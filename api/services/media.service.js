const AWS = require('aws-sdk');
const fs = require('fs');
const config = require('../config');


const getS3Client = () => {
    const s3 = new AWS.S3({
        accessKeyId: config.awsAccessKey,
        secretAccessKey: config.awsSecretAccessKey
    });

    return s3;
}

const upload = async (file) => {
    const s3 = getS3Client();
    // Setting up S3 upload parameters
    const fileToUpload = fs.readFileSync(file.path);
    const params = {
        Bucket: config.s3BucketName,
        Key: +(new Date()) + '_' + file.name,
        ContentType: file.type,
        Body: fileToUpload,
        ACL: 'public-read',
    };

    // Uploading files to the bucket
    return new Promise((resolve, reject) => {
        s3.upload(params, function (err, data) {
            if (err) {
                reject(err);
            }
            resolve(data.Location);
        });
    })
};

module.exports = {
    upload
}