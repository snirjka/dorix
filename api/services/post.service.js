const mediaService = require('./media.service');
const PostModel = require('../models/post.model');

const create = async ({ image, description }) => {
    try {
        const imageUrl = await mediaService.upload(image);
        const postModel = new PostModel({
            imageUrl,
            description,
        });
        const post = await postModel.save();
        return post;
    }
    catch (err) {
        throw err;
    }
}

const update = async (id, postObj) => {
    try {
        const post = await PostModel.findByIdAndUpdate(id, postObj);
        return post;
    }
    catch (err) {
        throw err;
    }
}

const remove = async (id) => {
    try {
        const post = await PostModel.findByIdAndRemove(id);
        return post;
    }
    catch (err) {
        throw err;
    }
}

const getAll = async (query) => {
    try {
        const posts = await PostModel.find(query).sort({'_id': -1});
        return posts;
    }
    catch (err) {
        throw err;
    }
}

module.exports = {
    create,
    update,
    remove,
    getAll
}