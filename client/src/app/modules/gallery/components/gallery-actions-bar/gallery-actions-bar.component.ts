import { Component, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { GalleyPostUploadDialogComponent } from '../galley-post-upload-dialog/galley-post-upload-dialog.component';

@Component({
  selector: 'app-gallery-actions-bar',
  templateUrl: './gallery-actions-bar.component.html',
  styleUrls: ['./gallery-actions-bar.component.scss']
})
export class GalleryActionsBarComponent {
  @Output() postsCreated = new EventEmitter();
  constructor(private dialog: MatDialog) { }


  openUploadDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
    };

    const dialogRef = this.dialog.open(GalleyPostUploadDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      status => {
        if (status) this.postsCreated.emit();
      }
    );
  }

}
