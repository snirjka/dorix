import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { GalleyPostUpdateDialogComponent } from '../galley-post-update-dialog/galley-post-update-dialog.component';

@Component({
  selector: 'app-gallery-post',
  templateUrl: './gallery-post.component.html',
  styleUrls: ['./gallery-post.component.scss']
})
export class GalleryPostComponent {
  @Input() post;
  @Output() postsUpdated = new EventEmitter();

  constructor(private dialog: MatDialog) { }


  openUpdateDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      ...this.post
    };

    const dialogRef = this.dialog.open(GalleyPostUpdateDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      status => {
        if (status) this.postsUpdated.emit();
      }
    );
  }

}
