import { Component, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-gallery-search-bar',
  templateUrl: './gallery-search-bar.component.html',
  styleUrls: ['./gallery-search-bar.component.scss']
})
export class GallerySearchBarComponent {
  @Output() onSearch = new EventEmitter();
  searchDebounce = new Subject<string>();

  constructor() {
    // Debounce search.
    this.searchDebounce.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        this.onSearch.emit(value);
      });
  }

  search(e) {
    const val = e.target.value;
    this.searchDebounce.next(val);
  }

}
