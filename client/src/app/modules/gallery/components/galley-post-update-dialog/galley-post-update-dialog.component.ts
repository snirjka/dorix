import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from '../../../../services/post.service';

@Component({
  selector: 'app-galley-post-update-dialog',
  templateUrl: './galley-post-update-dialog.component.html',
  styleUrls: ['./galley-post-update-dialog.component.scss']
})
export class GalleyPostUpdateDialogComponent implements OnInit {
  updateForm: FormGroup;
  data;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<GalleyPostUpdateDialogComponent>,
    private postService: PostService,
    @Inject(MAT_DIALOG_DATA) data) {
      this.data = data;
  }

  ngOnInit() {
    this.updateForm = this.fb.group({
      description: new FormControl(this.data.description, Validators.required),
    });
  }

  submit(e) {
    e.preventDefault();
    const description = this.updateForm.value.description;
    this.postService.updatePostDescription(description, this.data._id).subscribe(res => {
      this.close(true);
    });
  }

  deletePost() {
    this.postService.deletePost(this.data._id).subscribe(res => {
      this.close(true);
    });
  }

  close(data) {
    this.dialogRef.close(data);
  }

}
