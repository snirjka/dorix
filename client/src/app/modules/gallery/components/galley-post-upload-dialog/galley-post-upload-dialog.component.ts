import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from '../../../../services/post.service';

@Component({
  selector: 'app-galley-post-upload-dialog',
  templateUrl: './galley-post-upload-dialog.component.html',
  styleUrls: ['./galley-post-upload-dialog.component.scss']
})
export class GalleyPostUploadDialogComponent implements OnInit {
  uploadForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<GalleyPostUploadDialogComponent>,
    private postService: PostService,
    @Inject(MAT_DIALOG_DATA) data) {

  }

  ngOnInit() {
    this.uploadForm = this.fb.group({
      description: new FormControl(null, Validators.required),
      image: new FormControl(null, [Validators.required])
    });
  }

  onImageSelected(e) {
    const file = e.target.files[0];
    this.uploadForm.controls.image.setValue(file);
  }

  async submit(e) {
    e.preventDefault();
    this.postService.createPost(this.uploadForm.value).subscribe(res => {
      this.close(true);
    });
  }

  close(data) {
    this.dialogRef.close(data);
  }

}
