import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryRoutingModule } from './gallery-routing.module';
import { MaterialModule } from '../../material.module';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { GalleryPostComponent } from './components/gallery-post/gallery-post.component';
import { GalleryActionsBarComponent } from './components/gallery-actions-bar/gallery-actions-bar.component';
import { GalleyPostUploadDialogComponent } from './components/galley-post-upload-dialog/galley-post-upload-dialog.component';
import { GalleyPostUpdateDialogComponent } from './components/galley-post-update-dialog/galley-post-update-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GallerySearchBarComponent } from './components/gallery-search-bar/gallery-search-bar.component';
import { UploadComponent } from './pages/upload/upload.component';



@NgModule({
  declarations: [
    GalleryComponent,
    GalleryPostComponent,
    GalleryActionsBarComponent,
    GalleyPostUploadDialogComponent,
    GalleyPostUpdateDialogComponent,
    GallerySearchBarComponent,
    UploadComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    GalleryRoutingModule
  ],
  exports: [GalleryComponent],
  entryComponents: [GalleyPostUploadDialogComponent, GalleyPostUpdateDialogComponent]
})
export class GalleryModule { }
