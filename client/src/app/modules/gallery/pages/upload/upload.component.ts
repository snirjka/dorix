import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PostService } from 'src/app/services/post.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  uploadForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private postService: PostService) {

  }

  ngOnInit() {
    this.uploadForm = this.fb.group({
      description: new FormControl(null, Validators.required),
      image: new FormControl(null, [Validators.required])
    });
  }

  onImageSelected(e) {
    const file = e.target.files[0];
    this.uploadForm.controls.image.setValue(file);
  }

  async submit(e) {
    e.preventDefault();
    this.postService.createPost(this.uploadForm.value).subscribe(res => {
      this.router.navigate(['/']);
    });
  }
}
