import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const url = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getPosts(query) {
    return this.http.get(url + '/posts' + (query ? `?query=${query}` : ''));
  }

  createPost(post) {
    const fd = new FormData();
    fd.append('description', post.description);
    fd.append('image', post.image, post.image.name);
    return this.http.post(url + '/posts', fd);
  }

  updatePostDescription(description, id) {
    return this.http.get(url + '/posts/' + id + '?description=' + description);
  }

  deletePost(id) {
    return this.http.delete(url + '/posts/' + id);
  }

  private fileToBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}
